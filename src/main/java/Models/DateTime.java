package Models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime {
	
	String beginD;
	String finishD;
	public String totalTime;
	
	public DateTime(String a, String b)
	{
		beginD = a;//2011-12-07 14:03:34
		finishD = b;//2011-12-07 14:39:30
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		
		Date d1,d2;
		
		try {
			
			d1 = format.parse(beginD);
			d2 = format.parse(finishD);
			
			long diff = d2.getTime() - d1.getTime();
			
			long diffS = diff / 1000 % 60;
			long diffM = diff / (60 * 1000) % 60;
			long diffH = diff / (60 * 60 * 1000) % 24;
			long diffD = diff / (60 * 60 * 24 * 1000);

			totalTime = diffH + ":" + diffM + ":" + diffS ;
			
		}catch(Exception e){e.printStackTrace();}
		
	}
	
	public String updateTotalTimeString(String a, String b)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		SimpleDateFormat formatT = new SimpleDateFormat("HH:mm:ss");
		
		String zeroTime = "0:00:00";
		Date t1,t2,t;
		
		try {
			
			t = formatT.parse(totalTime);
			t1 = formatT.parse(zeroTime);
			
			long dif = t.getTime() - t1.getTime();
			
			long difS = dif/ 1000 % 60;
			long difM = dif/ (60*1000) % 60;
			long difH = dif/ (60*60*1000) % 24;
			
		
		
		Date d1, d2;
		String tempTime;
		
		
			d1 = format.parse(a);
			d2 = format.parse(b);
			
			long diff = d2.getTime() - d1.getTime();
			
			long diffS = diff / 1000 % 60;
			long diffM = diff / (60 * 1000) % 60;
			long diffH = diff / (60 * 60 * 1000) % 24;
			long diffD = diff / (60 * 60 * 24 * 1000);
		
		
		totalTime = (difH + diffH) + ":" + (difM + diffM) + ":" + (difS + diffS);
		
		}catch(Exception e) {e.printStackTrace();}
		
		return totalTime;
	}
	
	public DateTime updateTotalTime(String a, String b)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		SimpleDateFormat formatT = new SimpleDateFormat("HH:mm:ss");
		
		String zeroTime = "0:00:00";
		Date t1,t2,t;
		
		try {
			
			t = formatT.parse(totalTime);
			t1 = formatT.parse(zeroTime);
			
			long dif = t.getTime() - t1.getTime();
			
			long difS = dif/ 1000 % 60;
			long difM = dif/ (60*1000) % 60;
			long difH = dif/ (60*60*1000) % 24;
			
		
		
		Date d1, d2;
		String tempTime;
		
		
			d1 = format.parse(a);
			d2 = format.parse(b);
			
			long diff = d2.getTime() - d1.getTime();
			
			long diffS = diff / 1000 % 60;
			long diffM = diff / (60 * 1000) % 60;
			long diffH = diff / (60 * 60 * 1000) % 24;
			long diffD = diff / (60 * 60 * 24 * 1000);
		
		
		totalTime = (difH + diffH) + ":" + (difM + diffM) + ":" + (difS + diffS);
		
		}catch(Exception e) {e.printStackTrace();}
		
		return this;
	}

}
