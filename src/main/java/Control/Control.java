package Control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import Models.DateTime;
import Models.MonitoredData;


interface InterfataPopulare{
	public void populareDate();
}
interface CountActiv{
	public void countAct() throws IOException;
}
interface IcountDays{
	public void countDays() throws IOException;
}
interface IcountDaysInt{
	public int countDaysInt() throws IOException;
}
interface IactivPerDay{
	public void activitiesPerDay() throws IOException;
}
interface ItotalDuration{
	public void totalDuration() throws IOException;
}
interface IdurationLT5{
	public void durationLT5() throws IOException;
}

public class Control {
	//todo
	int dataI = 28;
	int count = 0;
	
	MonitoredData[] m = new MonitoredData[248];
	HashMap<String,Integer> countAct = new HashMap<String,Integer>();
	HashMap<String,DateTime> totalTime = new HashMap<String,DateTime>();
	List<String> list = new ArrayList<String>();
	
	////////////
	List<String> linii = new ArrayList<String>();
	List<MonitoredData> mD = new ArrayList<MonitoredData>();
	
	

	InterfataPopulare ppD = () -> 
	{
	String fileName = "activ.txt";
	

	try (Stream<String> stream = Files.lines(Paths.get(fileName))){

		list = stream.collect(Collectors.toList());
		
		int i = 0;
		while(i != list.size())
				{
			String[] arr = list.get(i).split("\t");
			//System.out.println(list.get(i));
			
			m[i] = new MonitoredData(arr[0],arr[2],arr[4]);
			i++;
				}
	}catch(Exception e1) {e1.printStackTrace();}
	
	
	
	//using streams
	try(Stream<String> str = Files.lines(Paths.get(fileName))){
		linii = 
		str
		.collect(Collectors.toList());

	
		
	}catch(Exception f) {f.printStackTrace();}
	
	mD = linii.stream()
			.peek(System.out::println)
			.map(
					temp ->
					{
					String[] arr = temp.split("\t");
					DateTime t = new DateTime(arr[0],arr[2]);
					System.out.println("Activitatea " + arr[4] + " are durata de : " + t.totalTime);
					return new MonitoredData(arr[0],arr[2],arr[4]);
					}).collect(Collectors.toList());
	
	
	System.out.println(mD.size());
	
	//System.out.println("finish with " + m.length + " objects");
	};
	
	CountActiv cA = () ->
	{
		List<String> s = new ArrayList<String>();
		
		s = mD.stream()
				.map(temp->
				{
					if(countAct.containsKey(temp.activity))
						countAct.put(temp.activity , countAct.get(temp.activity ) + 1);
					else
						countAct.put(temp.activity, 1);
					
					return "ceva";
				}).collect(Collectors.toList());
		
		
		//File path = new File("activities and occurence.txt");
		File path = new File("E:\\PT2018\\proj 5\\Project5\\activities and occurence.txt");
		BufferedWriter writer;
		writer = new BufferedWriter(new FileWriter(path,true));
		
		s = countAct.entrySet().stream()
				.map(temp ->
				{
					String act = temp.getKey();
					int nr = temp.getValue();
					
					String out = act + " : " + nr;
					String enter = "\n";
					
					try {
						writer.write(out);
						writer.newLine();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException f) {
						f.printStackTrace();
					}
					
					
					return "a";
				}).collect(Collectors.toList());
		
		
		writer.close();
		System.out.println();
	/*	for(int i = 0; i < m.length; i++)
		{
			if(countAct.containsKey(m[i].activity))
				countAct.put( m[i].activity , countAct.get( m[i].activity ) + 1);
			else
				countAct.put(m[i].activity, 1);
		}

		
		File path = new File("activities and occurence.txt");
		BufferedWriter writer;
			writer = new BufferedWriter(new FileWriter(path,true));

		
		//OutputStream OS = new FileOutputStream(path);
		
		for(Map.Entry<String, Integer> entry : countAct.entrySet())
			{
				String act = entry.getKey();
				int nr = entry.getValue();
				
				String out = act + " : " + nr;
				
				try {
					writer.write(out);
					writer.newLine();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException f) {
					f.printStackTrace();
				}
			}
		*/
		System.out.println("done writing occurence");
		//OS.close();
		//writer.close();
	};

	IcountDays cD = ()->
	{
		System.out.println();
		List<String> zile = new ArrayList<String>();
		
		
		zile = mD.stream()
				.map(
					temp->
					{
						String data = temp.startT;
						String nr = String.valueOf(new char[] {data.charAt(8),data.charAt(9)});
						int nrr = Integer.parseInt(nr);

						if(nrr != dataI)
						{
							dataI = nrr;
							count++;
						}
					return "ceva";	
					}).collect(Collectors.toList());
		count++;
		System.out.println("zile diferite = "+ count);

	};
	
	IcountDaysInt cDI = () ->
	{
		String data = m[0].startT;
		char[] c = {data.charAt(8),data.charAt(9)};
		String nr = String.valueOf(c);
		int nrr = Integer.parseInt(nr);//28
		int count = 1;
		
		for(int i = 1; i < m.length; i++)
		{
			
			data = m[i].startT;
			char[] ziC = {data.charAt(8),data.charAt(9)};
			nr = String.valueOf(ziC);
			int zi = Integer.parseInt(nr);
			
			if(zi != nrr)
			{
				nrr = zi;
				count++;
			}
			else
				continue;
		}
		
		return count;
	};
	
	IactivPerDay aPD = () ->
	{
		
		System.out.println();
		List<String> zile = new ArrayList<String>();
		HashMap<Integer,HashMap<String,Integer>> acPD = new HashMap<Integer,HashMap<String,Integer>>();
		HashMap<String,Integer> acT = new HashMap<String,Integer>();
		dataI = 28;
		count = 1;
		File path = new File("activitati pe zile.txt");
		BufferedWriter writer;
		writer = new BufferedWriter(new FileWriter(path,true));
		
		zile = mD.stream()
				.map(
					temp->
					{
						
						String data = temp.startT;
						String nr = String.valueOf(new char[] {data.charAt(8),data.charAt(9)});
						int nrr = Integer.parseInt(nr);

						if(dataI != nrr)
						{
							//aici se face scrierea si adaugarea in hashmap
							
							try {
								writer.write(String.valueOf(count));
								writer.newLine();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							for(Map.Entry<String, Integer> map : acT.entrySet())
							{
								String a = map.getKey();
								int b = map.getValue();
								
								try {
									writer.write(a + " : " + b);
									writer.newLine();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
							acPD.put(count,acT);
							acT.clear();
							
							//
							dataI = nrr;
							count++;
						}
						else
						{
							if(acT.containsKey(temp.activity))
								acT.put(temp.activity, acT.get(temp.activity) + 1);
							else
								acT.put(temp.activity,1);
						}		
					return "ceva";	
					}).collect(Collectors.toList());
		writer.close();
		System.out.println("done writing activ/day");
		
	/*	
		int max = countDaysInt();
		HashMap<Integer,HashMap<String,Integer>> acPD = new HashMap<Integer,HashMap<String,Integer>>();
		
		HashMap<String,Integer> acT = new HashMap<String,Integer>();
		
		String data = m[0].startT;
		char[] c = {data.charAt(8),data.charAt(9)};
		String nr = String.valueOf(c);
		int nrr = Integer.parseInt(nr);
		int count = 1;

		for(int i = 0; i < m.length; i++)
		{

			File path = new File("activitati pe zile.txt");
			BufferedWriter writer = new BufferedWriter(new FileWriter(path,true));

			data = m[i].startT;
			char[] ziC = {data.charAt(8),data.charAt(9)};
			nr = String.valueOf(ziC);
			int zi = Integer.parseInt(nr);
			
			if(zi != nrr || i == m.length - 1)
			{
				//aici se face scrierea si adaugarea in hashmap
				
				writer.write(String.valueOf(count));
				writer.newLine();
				for(Map.Entry<String, Integer> map : acT.entrySet())
				{
					String a = map.getKey();
					int b = map.getValue();
					
					writer.write(a + " : " + b);
					writer.newLine();
				}
				acPD.put(count,acT);
				acT.clear();
				
				//
				nrr = zi;
				count++;
			}
			else
			{
				if(acT.containsKey(m[i].activity))
					acT.put(m[i].activity, acT.get(m[i].activity) + 1);
				else
					acT.put(m[i].activity,1);
			}	
			writer.close();
		}*/
	};
	
	ItotalDuration tD = () ->
	{
		HashMap<String,DateTime> map = new HashMap<String,DateTime>();
		File path = new File("activitati cu timp mai mare de 10 ore.txt");
		BufferedWriter writer = new BufferedWriter(new FileWriter(path,true));

		
		
		for(int i = 0; i < m.length; i++)
		{
			if(map.containsKey(m[i].activity))
				map.put(m[i].activity,map.get(m[i].activity).updateTotalTime(m[i].startT, m[i].endT));
			else
				map.put(m[i].activity,new DateTime(m[i].startT, m[i].endT));
			
		}
		
		for(Map.Entry<String, DateTime> entry : map.entrySet())
		{
			String a = entry.getKey();
			DateTime b = entry.getValue();
			
			if(b.totalTime.charAt(1) != ':') {
			char[] timp = {b.totalTime.charAt(0),b.totalTime.charAt(1)};
			String timpS = new String(timp);
			
			if(Integer.parseInt(timpS) >= 10)
				writer.write(a + " cu timpul de : " + b.totalTime);
				writer.newLine();
			}
			System.out.println("Activitatea : " + a + " are timpul total de : " + b.totalTime);
		}
		writer.close();
		System.out.println();
		System.out.println("done writing duration > 10");
	};
	
	public void differentDay()
	{
		String data = m[0].startT;
		char[] c = {data.charAt(8),data.charAt(9)};
		String nr = String.valueOf(c);
		int nrr = Integer.parseInt(nr);//28
		int count = 1;
		
		for(int i = 1; i < m.length; i++)
		{
			
			data = m[i].startT;
			char[] ziC = {data.charAt(8),data.charAt(9)};
			nr = String.valueOf(ziC);
			int zi = Integer.parseInt(nr);
			
			if(zi != nrr)
			{
				nrr = zi;
				count++;
			}
			else
				continue;
			
		}
	}
	
	public void start() throws IOException, InterruptedException
	{
		//populareDate();
		/*Thread.sleep(1000);
		countAct();
		Thread.sleep(1000);
		countDays();
		Thread.sleep(1000);
		activitiesPerDay();
		Thread.sleep(1000);
		totalDuration();
		Thread.sleep(1000);
		*/
		ppD.populareDate();
		Thread.sleep(1000);
		cA.countAct();
		Thread.sleep(1000);
		cD.countDays();
		Thread.sleep(1000);
		aPD.activitiesPerDay();
		Thread.sleep(1000);
		totalDuration();
		Thread.sleep(1000);
		//tD.totalDuration();
		//Thread.sleep(1000);
		
		
	}

	public void durationLT5() throws IOException
	{
		HashMap<String,DateTime> map = new HashMap<String,DateTime>();
		File path = new File("activitati cu timp mai mare de 10 ore.txt");
		BufferedWriter writer = new BufferedWriter(new FileWriter(path,true));
		
		for(int i = 0; i < m.length; i++)
		{
			if(map.containsKey(m[i].activity))
				map.put(m[i].activity,map.get(m[i].activity).updateTotalTime(m[i].startT, m[i].endT));
			else
				map.put(m[i].activity,new DateTime(m[i].startT, m[i].endT));
			//todo
		}
	}
	
	public void totalDuration() throws IOException
	{
		HashMap<String,DateTime> map = new HashMap<String,DateTime>();
		File path = new File("activitati cu timp mai mare de 10 ore.txt");
		BufferedWriter writer = new BufferedWriter(new FileWriter(path,true));

		
		
		for(int i = 0; i < m.length; i++)
		{
			if(map.containsKey(m[i].activity))
				map.put(m[i].activity,map.get(m[i].activity).updateTotalTime(m[i].startT, m[i].endT));
			else
				map.put(m[i].activity,new DateTime(m[i].startT, m[i].endT));
			
		}
		
		for(Map.Entry<String, DateTime> entry : map.entrySet())
		{
			String a = entry.getKey();
			DateTime b = entry.getValue();
			
			if(b.totalTime.charAt(1) != ':') {
			char[] timp = {b.totalTime.charAt(0),b.totalTime.charAt(1)};
			String timpS = new String(timp);
			
			if(Integer.parseInt(timpS) >= 10)
				writer.write(a + " cu timpul de : " + b.totalTime);
				writer.newLine();
			}
			System.out.println("Activitatea : " + a + " are timpul total de : " + b.totalTime);
		}
		writer.close();
		//System.out.println();
		//System.out.println("done writing duration > 10");
	}
	
	public void activitiesPerDay() throws IOException
	{
		int max = countDaysInt();
		HashMap<Integer,HashMap<String,Integer>> acPD = new HashMap<Integer,HashMap<String,Integer>>();
		
		HashMap<String,Integer> acT = new HashMap<String,Integer>();
		
		String data = m[0].startT;
		char[] c = {data.charAt(8),data.charAt(9)};
		String nr = String.valueOf(c);
		int nrr = Integer.parseInt(nr);
		int count = 1;

		for(int i = 0; i < m.length; i++)
		{

			File path = new File("activitati pe zile.txt");
			BufferedWriter writer = new BufferedWriter(new FileWriter(path,true));

			data = m[i].startT;
			char[] ziC = {data.charAt(8),data.charAt(9)};
			nr = String.valueOf(ziC);
			int zi = Integer.parseInt(nr);
			
			if(zi != nrr || i == m.length - 1)
			{
				//aici se face scrierea si adaugarea in hashmap
				
				writer.write(String.valueOf(count));
				writer.newLine();
				for(Map.Entry<String, Integer> map : acT.entrySet())
				{
					String a = map.getKey();
					int b = map.getValue();
					
					writer.write(a + " : " + b);
					writer.newLine();
				}
				acPD.put(count,acT);
				acT.clear();
				
				//
				nrr = zi;
				count++;
			}
			else
			{
				if(acT.containsKey(m[i].activity))
					acT.put(m[i].activity, acT.get(m[i].activity) + 1);
				else
					acT.put(m[i].activity,1);
			}	
			writer.close();
		}
	}
	
	public int countDaysInt() throws IOException
	{
		String data = m[0].startT;
		char[] c = {data.charAt(8),data.charAt(9)};
		String nr = String.valueOf(c);
		int nrr = Integer.parseInt(nr);//28
		int count = 1;
		
		for(int i = 1; i < m.length; i++)
		{
			
			data = m[i].startT;
			char[] ziC = {data.charAt(8),data.charAt(9)};
			nr = String.valueOf(ziC);
			int zi = Integer.parseInt(nr);
			
			if(zi != nrr)
			{
				nrr = zi;
				count++;
			}
			else
				continue;
		}
		
		return count;
	}
	
	public void countDays() throws IOException
	{
		System.out.println();
		
		String data = m[0].startT;
		char[] c = {data.charAt(8),data.charAt(9)};
		String nr = String.valueOf(c);
		int nrr = Integer.parseInt(nr);//28
		int count = 1;
		
		for(int i = 1; i < m.length; i++)
		{
			
			data = m[i].startT;
			char[] ziC = {data.charAt(8),data.charAt(9)};
			nr = String.valueOf(ziC);
			int zi = Integer.parseInt(nr);
			
			if(zi != nrr)
			{
				nrr = zi;
				count++;
			}
			else
				continue;
			
		}
		System.out.println("zile diferite  = " +count);
	}
	
	public void countAct() throws IOException
	{
		System.out.println();
		for(int i = 0; i < m.length; i++)
		{
			if(countAct.containsKey(m[i].activity))
				countAct.put( m[i].activity , countAct.get( m[i].activity ) + 1);
			else
				countAct.put(m[i].activity, 1);
		}

		
		File path = new File("activities and occurence.txt");
		BufferedWriter writer = new BufferedWriter(new FileWriter(path,true));
		//OutputStream OS = new FileOutputStream(path);
		
		for(Map.Entry<String, Integer> entry : countAct.entrySet())
			{
				String act = entry.getKey();
				int nr = entry.getValue();
				
				String out = act + " : " + nr;
				
				try {
					writer.write(out);
					writer.newLine();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				
			}
		
		System.out.println("done writing occurence");
		//OS.close();
		writer.close();
	}
	
	public void apopulareDate() 
	{
		
		String fileName = "activ.txt";
		List<String> list = new ArrayList<String>();
		
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))){

			list = stream.collect(Collectors.toList());
			
			int i = 0;
			while(i != list.size())
					{
				String[] arr = list.get(i).split("\t");
				System.out.println(list.get(i));
				
				m[i] = new MonitoredData(arr[0],arr[2],arr[4]);
				i++;
					}
			System.out.println("finish with " + m.length + "objects");
			
		}catch(Exception e) {e.printStackTrace();}
		
		/*File file = new File("activ.txt");
		int i = 0;
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String line =  reader.readLine();
			
			
			while(line != null) {
				String[] arr = line.split("\t");
				
				System.out.println(line);
				
				m[i] = new MonitoredData(arr[0],arr[2],arr[4]);
				i++;
				line = reader.readLine();
			}
			reader.close();
			
			System.out.println("obiecte create : " + m.length + "/" + i);

		}catch(Exception e){System.out.println("eroare");}
*/
	}
	
}
